<?php

/*
 * Template Name: Home
*/

    get_header();
?>

<main class="main">
    <header class="js-header site-header">
        <div class="site-header__inner">
            <img class="logo-slogan" src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/conquer-the-summit.svg" alt="Conquer the Summit 2019" />
            <img class="logo-brand" src="<?php echo get_template_directory_uri(); ?>/assets//images/logos/proofpoint-logo.svg" alt="proofpoint" />
            
            <div class="intro">
                <?php echo get_field('introduction'); ?>
            </div>
        </div>

        <nav class="nav">
            <p class="nav__title">Climb the mountain</p>
            <?php if(have_rows('sections')) : ?>
                <ul class="nav__items">
                    <?php $i = 1; ?>
                    <?php while(have_rows('sections')) : ?>
                        <?php the_row(); ?>

                        <li class="nav__item nav__item--q<?php echo $i ?>">
                            <a class="js-section-trigger <?php if($i == 1) echo 'active'; ?>" href="#q<?php echo $i ?>">Q<?php echo $i ?></a></li>    
                        <li class="nav__item-breaker"></li>

                        <?php $i++; ?>
                    <?php endwhile ?>
                </ul>
            <?php endif ?>
        </nav>
    </header>

    <?php if(have_rows('sections')) : ?>
        <div class="js-sections section-container">

            <?php $i = 1; ?>
            <?php while(have_rows('sections')) : ?>
                <?php the_row(); ?>

                <section id="q<?php echo $i ?>" class="js-section section section--q<?php echo $i ?> <?php if($i == 1) echo 'active'; ?>">

                    <?php if(!get_sub_field('section_toggle')) : ?>
                        <div class="section__overlay">
                            <p>Coming Soon</p>
                        </div>
                    <?php endif ?>

                    <?php if(have_rows('section_tabs')) : ?>
                        <ul class="js-blocks link-blocks active">
                            <?php $j = 1; ?>
                            <?php while(have_rows('section_tabs')) : ?>
                                <?php the_row(); ?>
                                    <li class="link-block">
                                        <a href="#q<?php echo $i ?>-<?php echo $j ?>" class="js-block">
                                            <img class="link-block__icon" src="<?php echo get_sub_field('section_tab_icon') ?>" alt="" />
                                            <span><?php echo get_sub_field('section_tab_title'); ?></span>
                                        </a>
                                    </li>
                                <?php $j++; ?>
                            <?php endwhile ?>
                        </ul>

                        <div class="js-section-content section__content">
                            <nav class="section__nav">
                                <ul class="tabs">
                                    <?php $j = 1; ?>
                                    <?php while(have_rows('section_tabs')) : ?>
                                        <?php the_row(); ?>                                    
                                        <li class="<?php if($j == 1) echo 'active'; ?>"><a href="#q<?php echo $i ?>-<?php echo $j ?>" data-tab="#q<?php echo $i ?>-<?php echo $j ?>"><img class="tabs__icon" src="<?php echo get_sub_field('section_tab_icon') ?>" alt="" /></a></li>
                                        <?php $j++ ?>
                                    <?php endwhile ?>
                                </ul>
                                <a class="js-tabs-close section__nav__close"><i class="icon icon--x"></i></a>
                            </nav>

                            <div class="tab-content">
                                <?php $j = 1; ?>
                                <?php while(have_rows('section_tabs')) : ?>
                                    <?php the_row(); ?>                                   
                                    <div id="q<?php echo $i ?>-<?php echo $j ?>" class="tab-pane <?php if($j == 1) echo 'active'; ?>">
                                        <div class="entry">
                                            <?php echo get_sub_field('section_tab_content') ?>
                                        </div>
                                    </div>
                                    <?php $j++ ?>
                                <?php endwhile ?>
                            </div>  
                        </div>
                    <?php endif ?>
                </section>

                <?php $i++ ?>
            <?php endwhile ?>
        </div>
    <?php endif ?>
</main>

<div class="js-hero hero"> <!-- Added scroll-behaviour style for smooth scroll polyfill -->
    <div class="hero__inner">
        <div class="js-junction hero__junction hero__junction-q1" data-junction="#q1" data-colour="#00a090"></div>
        <div class="js-junction hero__junction hero__junction-q2" data-junction="#q2" data-colour="#235ba6"></div>
        <div class="js-junction hero__junction hero__junction-q3" data-junction="#q3" data-colour="#bf338a"></div>
        <div class="js-junction hero__junction hero__junction-q4" data-junction="#q4" data-colour="#e32213"></div>

        <svg class="navigator" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="651.06px" height="1692.17px" viewBox="27.82 -110.19 651.06 1692.17" style="enable-background:new 27.82 -110.19 651.06 1692.17;" xml:space="preserve">
            <style>
                #js-line-q1,
                #js-line-q2,
                #js-line-q3 {
                    fill:none;
                    stroke:#FFFFFF;
                    stroke-width:4;
                    stroke-miterlimit:10;
                    stroke-dasharray: 15;
                    stroke-dashoffset: 15;
                }

                <![CDATA[
                    @font-face {
                        font-family: "Tungsten-Semibold";
                        src: url(<?php echo get_template_directory_uri(); ?>/assets/fonts/Tungsten-Semibold.woff2);
                    }
                ]]>                
            </style>        
        
            <path id="js-line-q3" class="line" d="M504.82,504.12 L350.17,60.66"  mask="url(#line-master-mask-q3)" />
            <mask id="line-master-mask-q3">
                <path id="js-line-mask-q3" class="line" d="M504.82,504.12 L350.17,60.66" fill="none" stroke="#fff" stroke-linecap="round" stroke-miterlimit="10" stroke-width="8" />
            </mask>

            <path id="js-line-q2" class="line" d="M182.77,977.22 l315.59-447.09"  mask="url(#line-master-mask-q2)" />
            <mask id="line-master-mask-q2">
                <path id="js-line-mask-q2" class="line" d="M182.77,977.22 l315.59-447.09" fill="none" stroke="#fff" stroke-linecap="round" stroke-miterlimit="10" stroke-width="8"/>                
            </mask>

            <path id="js-line-q1" class="line" d="M340.24,1442.87l-158.36-439.08" mask="url(#line-master-mask-q1)" />
            <mask id="line-master-mask-q1">
                <path id="js-line-mask-q1" d="M340.24,1442.87l-158.36-439.08" fill="none" stroke="#fff" stroke-linecap="round" stroke-miterlimit="10" stroke-width="8" />
            </mask>

            <g id="js-navigator">
                <a class="js-navigator-next navigator__nav" href="#q2" data-section="2"><polyline style="fill:none;stroke:#FFFFFF;stroke-miterlimit:10;" points="316.14,1370.13 343.43,1342.84 370.72,1370.13 	"/></a>
                <a class="js-navigator-prev navigator__nav" href="#q1" data-section="1"><polyline style="fill:none;stroke:#FFFFFF;stroke-miterlimit:10;" points="370.72,1538.18 343.43,1565.47 316.14,1538.18 	"/></a>
                <line style="fill:none;stroke:#FFFFFF;stroke-miterlimit:10;" x1="208.01" y1="1454.89" x2="478.85" y2="1454.89"/>
                <g>
                    <path style="fill:#E22826;" d="M409.35,1454.89c0,36.41-29.51,65.92-65.92,65.92s-65.92-29.52-65.92-65.92
                        c0-36.4,29.51-65.92,65.92-65.92S409.35,1418.48,409.35,1454.89"/>
                    <circle id="js-navigator-target" style="fill:none;stroke:#FFFFFF;stroke-miterlimit:10;" cx="343.43" cy="1454.89" r="65.92"/>
                </g>
                <text id="js-navigator-text" class="navigator__text" x="-6" y="7" transform="matrix(1 0 0 1 310.7832 1483.7852)">Q4</text>
                <g>
                    <line style="fill:none;stroke:#FFFFFF;stroke-miterlimit:10;" x1="478.85" y1="1469.69" x2="453.16" y2="1469.69"/>
                    <line style="fill:none;stroke:#FFFFFF;stroke-miterlimit:10;" x1="233.7" y1="1440.3" x2="208.01" y2="1440.3"/>
                    <line style="fill:none;stroke:#FFFFFF;stroke-miterlimit:10;" x1="233.7" y1="1469.69" x2="208.01" y2="1469.69"/>
                    <line style="fill:none;stroke:#FFFFFF;stroke-miterlimit:10;" x1="478.85" y1="1440.3" x2="453.16" y2="1440.3"/>
                </g>
            </g>            
        </svg>

    </div>
    
</div>

<?php get_footer(); ?>