<?php

    /**
     * The default template for a page.
     *
     * @package     WordPress
     * @subpackage  Proofpoint
     * @since       Proofpoint 1.0
     */

    get_header();
?>

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : ?>
        <?php the_post(); ?>
        <?php the_content(); ?>
    <?php endwhile ?>
<?php endif ?>

<?php get_footer(); ?>