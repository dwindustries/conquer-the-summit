import { scrollIntoView } from 'scroll-js';

/* 
 * Using GSAP animations, move our element to it's 
 * corresponding scroll position, based on the selected Junction ID.
 */ 

function Navigator() {
    var DOM = {}

    function cacheDOM(){
        DOM = {
            navigator: "#js-navigator",
            navTarget: document.querySelector("#js-navigator-target"),
            navText: document.querySelector("#js-navigator-text"),
            hero: document.querySelector('.js-hero'),
            junctionEls: document.querySelectorAll('.js-junction')
        }
    }

    function getJunctionID(els, index){
        return els[index].dataset.junction;
    }

    function getCurrentIndex(activeEl){
        // Get ID of the current active element from the href
        const activeID = activeEl.getAttribute("href");
        return activeID.charAt(2) - 1;
    }

    function changeNavText(props){
        const junctionID = getJunctionID(props.DOM.junctionEls, props.newID);
        let navContent = junctionID.substr(1);
        props.DOM.navText.innerHTML = navContent.toUpperCase();
    }

    function changeElementColour(target, colour, timeline){
        // Change colour of element to given colour with GSAP animation
        timeline.to(target, 1, {fill: colour, ease: Power1.easeOut}, "0");    
    }

    function getActiveElementID(activeEl){
        // Get ID of the current active element from the href
        let href = activeEl.getAttribute("href");
        return href.charAt(2) - 1;
    }
           
    function runAnimations(props){
        // Runs the GSAP animations -> moves navigator el, and draw/undraw pathways
        const activeID = getActiveElementID(props.activeEl),
              junctionID = getJunctionID(props.DOM.junctionEls, props.newID),
              diff = props.newID - activeID; // Get difference between new & current ID
        let counter = 0;

        /* 
         * Loop and run anim for each step, from current active state ID
         * to the new state ID. Stepping up or down depending on neg or pos.
         */
        if(activeID !== props.newID){
            if(Math.sign(diff) === -1) {
                // If negative move el backwards
                for (let i = activeID - 1; i >= activeID + diff; i--) {
                    setTimeout(()=> {
                        traveller(i + 1, props.DOM.navigator, props.colour, null, true);
                    }, 500 * counter);
    
                    counter++;
                }
            } else {
                // If positive move el forwards
                for (let i = activeID + 1; i <= activeID + diff; i++) {
                    setTimeout(() => {
                        traveller(
                            i, 
                            props.DOM.navigator, 
                            props.colour, 
                            props.DOM.navTarget, 
                            false
                        );
                    }, 500 * counter);

                    counter++;
                }
            } 
            
            // Now let's move the element scroll position inline with 
            // the selected element
            manipulateScrolling(junctionID, props.DOM.hero, diff);
        }
    }

    function traveller(id, el, colour, target, backwards){
        var pathData = MorphSVGPlugin.pathDataToBezier("#js-line-mask-q"+id,{align: el, offsetY:-100, offsetX:-130});
        var tween = new TimelineMax();
    
        if(backwards){            
            tween.to(el, .5, {bezier:{values:pathData, type:"cubic"}}, "0")
                .fromTo("#js-line-mask-q"+id, .5, {drawSVG:"0% 0%"}, {drawSVG:"0% 100% ", ease:Linear.easeOut}, "0")
                .reverse(0);
        } else {
            tween.to(el, .5, {bezier:{values:pathData, type:"cubic"}}, "0")
                .fromTo("#js-line-mask-q"+id, .5, {drawSVG:"0% 0%"}, {drawSVG:"0% 100% ", ease:Linear.easeOut}, "0")
                .to(target, 1, {fill: colour, ease:Power1.easeOut}, "0");
        }
    }
   
    function manipulateScrolling(junctionId, hero, diff){
        // Animate scroll position to a given Junction ID
        // Uses a DOM element pre positioned with CSS
        const speed = (junctionId == "#q1") ? 2000 : Math.abs(diff * 1.5) * 500;
        const junction = document.querySelector("[data-junction='"+junctionId+"']");
        
        scrollIntoView(junction, hero, { duration: speed, easing: 'ease-out' });
    } 

    function render(props){
        // Replace our el text with new junction number
        changeNavText(props);

        // Replace colour to reflect new section
        changeElementColour(props.DOM.navTarget, props.colour, new TimelineMax());
        
        // Now run the GSAP element travel & scroll anims
        runAnimations(props);
    }

    function init(activeEl, newID, colour){
        cacheDOM();
        render({ DOM, activeEl, newID, colour });
    }
    
    return { init }
}

export default new Navigator