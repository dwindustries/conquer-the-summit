import getClosest from "../utils/getClosest";

export default function navTabs() {
    let contentSections = document.querySelectorAll('.js-section-content');    
    
    // Hide all content sections by default on load
    for (let i = 0; i < contentSections.length; i++) {
        contentSections[i].style.display = 'none';
    }    

    // Section Link Blocks 
    const blocks = document.querySelectorAll('.js-block');

    // Link Block Button click
    for (let i = 0; i < blocks.length; i++) {
        blocks[i].addEventListener("click", (event) => {
            event.preventDefault();
            linkBlockTrigger(event);
        }, true);
    }

    function linkBlockTrigger(linkBlockEvent){
        let btn = linkBlockEvent.currentTarget;
        let container = getClosest(linkBlockEvent.currentTarget, '.js-section');
        let lbs = container.querySelectorAll('.js-blocks'); // link block sections
        let cs = container.querySelectorAll('.js-section-content'); // content sections
        
        // Hide the link block sections in order to show content sections
        for (let i = 0; i < lbs.length; i++) {
            lbs[i].classList.remove("active");
            lbs[i].style.display = 'none';
        }

        // Show the Content sections within this container
        for (let i = 0; i < cs.length; i++) {
            cs[i].classList.add("active");
        }
       
        // now get the selected section by the href, hide others and show this one
        let tabLink = btn.getAttribute('href'),
            tabs = container.querySelectorAll('.tabs li'),
            tabPanes = container.querySelectorAll('.tab-pane'),
            tabBtnActive = container.querySelector("[data-tab='"+tabLink+"']"),
            tabPaneActive = container.querySelector(tabLink);

        // hide all tabs
        for (let i = 0; i < tabs.length; i++) {
            tabs[i].classList.remove("active");
            tabPanes[i].classList.remove("active");
        }        
        // Reveal selected tab with active class
        tabBtnActive.parentNode.classList.add('active');
        tabPaneActive.classList.add('active');
    }

    // Tab navigation for tab panes
    const tbs = document.querySelectorAll(".tabs li a"); // tab buttons

    // Inner section nav tab buttons
    for (let i = 0; i < tbs.length; i++) {
        tbs[i].addEventListener("click", (event) => {
            event.preventDefault();
            tabSectionToggle(event);
        }, true);
    }

    function tabSectionToggle(tabClickEvent) {
        let tabActive = tabClickEvent.currentTarget,
            tabLink = tabActive.getAttribute('href'),
            container = getClosest(tabActive, '.js-section'),
            tabs = container.querySelectorAll('.tabs li'), // section tabs
            tpanes = container.querySelectorAll('.tab-pane'),
            tPaneActive = container.querySelector(tabLink);

        // close all tabs in this section & apply selected active class
        for (let i = 0; i < tabs.length; i++) {
            tabs[i].classList.remove("active");
            tpanes[i].classList.remove("active");
        }
        tabActive.parentNode.classList.add("active");
        tPaneActive.classList.add('active');       
    }  

    let tabsClose = document.querySelectorAll('.js-tabs-close');   
    for (let i = 0; i < tabsClose.length; i++) {
        tabsClose[i].addEventListener("click", (event) => {
            event.preventDefault();
            toggleTabsClose(event);
        }, true);   
    }
        
    function toggleTabsClose(tabsEvent){
        let btn = tabsEvent.currentTarget,
            container = getClosest(btn, '.js-section'),
            lbs = container.querySelectorAll('.js-blocks'), // link block sections
            cs = container.querySelectorAll('.js-section-content'); // content sections
        
        // Hide the link block sections in order to show content sections
        for (let i = 0; i < lbs.length; i++) {
            lbs[i].classList.add("active");
            lbs[i].style.display = 'flex';
        }

        // Show the Content sections within this container
        for (let i = 0; i < cs.length; i++) {
            cs[i].classList.remove("active");
        }    
    }
}