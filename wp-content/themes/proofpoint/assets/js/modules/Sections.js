import Navigator from "../modules/Navigator";

function Sections() {
    var DOM = {}

    function cacheDOM(){
        DOM = {
            sections: document.querySelectorAll('.js-section'),
            sectionBtns: document.querySelectorAll(".js-section-trigger")
        }   
    }

    // Close all sections by default (CSS will keep first section open)
    function closeSections(els){
        for (let i = 0; i < els.length; i++) {
            els[i].style.display = 'none';
        }
    }

    // Click main nav tab button and run callback func
    function bindEvents(els){
        for (let i = 0; i < els.length; i++) {
            els[i].addEventListener("click", (event) => {
                event.preventDefault();

                // Toggle Sections within render function
                render(event, i); // pass the trigger number as param to update state

            }, true);
        } 
    }

    function initScrolling(currentActiveTrigger, activeSectionId, newSectionID){
        // Scroll hero container & navigator to defined junctions 
        let junction = document.querySelector("[data-junction='"+activeSectionId+"']"),
            colour = junction.dataset.colour;

        // Move the nav element up/down the screen & change colour
        Navigator.init( currentActiveTrigger, newSectionID, colour);
    }    

    function hideAllContentSections(els){
        for (let i = 0; i < els.length; i++) {
            els[i].style.display = 'none';
            els[i].classList.remove("active");
        }
    }    

    function activateLinkBlockSection(els){
        for (let i = 0; i < els.length; i++) {
            addActiveClass(els[i]);
            els[i].style.display = 'flex';
        }
    }    

    function addActiveClass(el){
        el.classList.add("active");
    }    

    function removeActiveClasses(els){
        for (let i = 0; i < els.length; i++) {
            els[i].classList.remove("active");
        }        
    }    

    function render(eventTrigger, triggerID) {
        const currentActiveTrigger = document.querySelector('.js-section-trigger.active'),
            newTrigger = eventTrigger.currentTarget, 
            activeSectionId = newTrigger.getAttribute("href"), // get the anchor
            activeSection = document.querySelector(activeSectionId), // get active section by id
            contentSections = activeSection.querySelectorAll('.js-section-content'), // Get section content areas within container
            linkBlockSections = activeSection.querySelectorAll('.js-blocks'); // Section Link Blocks 

        // Remove active from all section triggers
        removeActiveClasses(DOM.sectionBtns);
        
        // Add active to selected trigger
        addActiveClass(newTrigger);

        // Remove active from all sections
        removeActiveClasses(DOM.sections);

        // Add active to selected section
        addActiveClass(activeSection);

        // Let's pass our data to the Navigator to make 
        // our hero element move and scroll element up and down
        initScrolling(currentActiveTrigger, activeSectionId, triggerID);

        // Hide all content tabs within this section
        // As we want to show its initial screen link blocks initially
        hideAllContentSections(contentSections);

        // So now let's show the first screen of our new section
        activateLinkBlockSection(linkBlockSections);
    }    

    function init(){
        cacheDOM();
        closeSections(DOM.sections);
        bindEvents(DOM.sectionBtns);
    }

    return { init }
}

export default new Sections