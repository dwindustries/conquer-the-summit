function Hero() {
    var DOM = {}

    function cacheDOM(){
        DOM = {
            hero: document.querySelector('.js-hero'),
            headerH: document.querySelector('.js-header'),
            sectionsH: document.querySelector('.js-sections')
        }
    }
            
    function render(){
        var windowH = window.innerHeight;
        var heroH = DOM.windowH - (DOM.headerH.clientHeight + DOM.sectionsH.clientHeight);

        if(DOM.windowH < 700 && window.innerWidth < 900){
            heroH = 700 - (DOM.headerH.clientHeight + DOM.sectionsH.clientHeight);
        }
        
        if(window.innerWidth < 900){
            DOM.hero.style.height = heroH + 'px';
            DOM.hero.style.bottom = DOM.sectionsH.clientHeight + 'px';  
        }

        if(window.innerWidth < 600){
            DOM.hero.scrollTop = 760;    
        } else if(window.innerWidth > 600 && window.innerWidth < 900) {
            DOM.hero.scrollTop = 1500;  
        } else {
            DOM.hero.scrollTop = 2800;  
        }
    }
    
    function init(){    
        cacheDOM();
        render();    
    }

    return { init }
}

export default new Hero