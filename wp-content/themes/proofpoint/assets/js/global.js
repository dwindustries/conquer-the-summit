"use strict";

import Sections from "./modules/Sections";
import navTabs from "./modules/navTabs";
import Hero from "./modules/Hero";
import Navigator from "./modules/Navigator";
import fadeOut from "./utils/fadeOut";

// Init our site functions
document.addEventListener("DOMContentLoaded", (event) => {     

    navTabs();
    Sections.init();
    
}, {passive: true});

window.onload = function(){

    Hero.init();

    let activeBtn = document.querySelector('.js-section-trigger.active')
    Navigator.init( activeBtn, 0, "#00a090" );

    TweenLite.set("#js-navigator", {transformOrigin:"50% 50%"});
    TweenMax.set("#js-line-mask-q1", {drawSVG:"0% 0%"});
    TweenMax.set("#js-line-mask-q2", {drawSVG:"0% 0%"});
    TweenMax.set("#js-line-mask-q3", {drawSVG:"0% 0%"});    

    const overlay = document.querySelector('.js-site-overlay');
    setTimeout(() => {
        fadeOut(overlay);
    }, 2000);

};