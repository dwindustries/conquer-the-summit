<?php
	/**
	 * The Header for our theme
	 *
	 * Displays the <head> and <header> sections
	 *
	 * @package WordPress
	 * @subpackage Proofpoint
	 * @since Proofpoint 1.0
	 */
?>

<!doctype html>
<html class="no-js" lang="">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Proofpoint</title>
	<meta name="description" content="">
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">

	<link rel="icon" href="<?php echo get_stylesheet_directory_uri() . '/assets/images/icons/favicon.ico'; ?>" type="image/x-icon" />

	<script type="text/javascript">
		// Apply js class to html
        document.documentElement.className = 'js';     
	</script>
	<style type="text/css">
		/*Hide nav on page load if js available*/
        /* .js .accordion__item { display:none; } */
	</style>

	<!--[if lt IE 9]>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
	<![endif]-->

	<?php wp_head(); ?>
</head>
<body>
    <div class="site-wrapper">
        <div class="js-site-overlay overlay">
            <img class="overlay__brand" src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/conquer-the-summit.svg" alt="Conquer the Summit 2019" />        
        </div>
